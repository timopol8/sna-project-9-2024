# SNA Project 9 2024

Project 9. Twitter COVID19 Network Analysis III


Consider the TweetsCOV19 dataset, a semantically annotated corpus of Twitter about COVID19
aiming at capturing online discourse about various aspects of the pandemic and its social impact.
The dataset is available from TweetsCOV19 - A Semantically Annotated Corpus of Tweets About the COVID-19
Pandemic (Part 1, October 2019 - April 2020) (https://zenodo.org/records/3871753). The dataset includes attributes related to Twitter
Id, timestamp, sentiment, mention, hashtag, entities, among others.

1. Use the timestamp attribute to restrict the collection to Twitter data in the period January
2020-March 2020.

2. We want to construct a network according to the associated mention, where nodes
correspond to Twitter Ids, and an edge between two nodes is established if the
corresponding twitter ids share the same mention in their Mention string attributes. Write a
script that implements this graph construction. What is the size of the graph in terms of
number of nodes and edges. Save the adjacency matrix of this graph in

3. Write a script that uses NetworkX to identify the largest component, second largest and
third largest component of the network as well as the average path length.

4. Write a script that plots the degree centrality distribution and cumulative degree centrality
distribution.

5. We want to test the extent to which the degree centrality distributions in 3) fit a power law
distribution. You may inspire from the implementation in powerlaw · PyPI of the power-law
distribution, or can use alternative one of your choice. It is important to quantify the
goodness of fit using p-value. Typically, when p-value is greater than 10%, we can state that
power-law is a plausible fit to the (distribution) data.

6. We want to capitalize on the information about the sentiment of tweet. Write a script that
calculates the overal sentiment of each tweet by adding the positive and negative sentiment
score available in the sentiment attribute.

7. Now we want to evaluate the extent to which positive (resp. negative) sentiment tweet
connects with positive (resp. negative) sentiment tweet. Suggest a script that traverses all
edges and calculates the proportion of positive-positive connection, negative-negative
connection and positive-negative or negative-positive connection.

8. Now we want to take into account the time evolution. For this purpose, use the timestamp
of the tweets. Suggest a script that creates some time subdivision (by assigning a fixed
number of subdivisions between the largest and smallest recorded time of the tweets) and
calculates the evolution of the total number of positive sentiment tweets after each time
increment (subdivision). Suggest a parametric distribution (i.e., polynomial or exponential)
that best fits the obtained graph (using curve fitting method of your choice).
9. Repeat 8) when using negative sentiment tweets.

10. We want to assimilate negative sentiment propgation to a virus propagation case. Use the
NDLIB library to simulate an SIS propagation model where the overall time is given by the
number of subdivisions in 8), and the graph is set by the graph configuration at time
increment 1 (after first subdivision). Select several choices for parameters lambda and beta
to generate contamination (total number of negative sentiments) close to that observed in 9)
(although no guarantee that this can be achieved). Draw the simulation showing the number
of contamination (number of tweets with negative sentiment) over time, and compare this
with that obtained in 8).
11. Suggest appropriate litearture to support the findings and comment on the limitations of the
overal reasoning pipeline.